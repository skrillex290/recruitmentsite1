'use strict';

describe('Controller: SubmitcvCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var SubmitcvCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SubmitcvCtrl = $controller('SubmitcvCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
