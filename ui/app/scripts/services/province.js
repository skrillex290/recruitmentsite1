'use strict';

/**
 * @ngdoc service
 * @name uiApp.Job
 * @description
 * # Province
 * Service in the uiApp.
 */


 /*
  *
  app.factory('Person', function($resource) {
return $resource("http://www.filltext.com/?rows=50&fname={firstName}&lname={lastName}&tel={phone|format}&address={streetAddress}&city={city}&state={usState|abbr}&zip={zip}");
});
  * */
angular.module('uiApp')
  .service('ProvinceService', ['$resource',function ($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    return $resource("http://localhost/rsc/api/province/:id",
    {},
    {
      query:
      {
        method:'GET',
        transformResponse: function(data)
            {
              console.log('In query');
              return JSON.parse(data).result;
            },
          isArray: true
      },
      get: {
          method:'GET',
          transformResponse: function(data)
              {
                console.log('In get');
                console.log(JSON.parse(data).result);
                return JSON.parse(data).result;
              },
            isArray: false
      }
    });
  }]);
