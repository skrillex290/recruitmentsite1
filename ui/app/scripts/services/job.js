'use strict';

/**
 * @ngdoc service
 * @name uiApp.Job
 * @description
 * # Job
 * Service in the uiApp.
 */


 /*
  *
  app.factory('Person', function($resource) {
return $resource("http://www.filltext.com/?rows=50&fname={firstName}&lname={lastName}&tel={phone|format}&address={streetAddress}&city={city}&state={usState|abbr}&zip={zip}");
});
  * */
angular.module('uiApp')
  .service('JobService', ['$resource',function ($resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    return $resource("http://localhost/rsc/api/jobs");    
  }]);
