'use strict';

/**
 * @ngdoc overview
 * @name uiApp
 * @description
 * # uiApp
 *
 * Main module of the application.
 */
angular
  .module('uiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap'
  ])
//---------------------------------------------------
//var app=angular.module('uiApp');
//---------------------------------------------------
.config(['$stateProvider','$locationProvider',function($stateProvider,$locationProvider)
{
  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');

   $stateProvider.state("index", {
		url:"/index",
		views: {
			"viewA":{
				templateUrl:"http://localhost/rsc/ui/views/main.html"
				}
			}
		    }
	)
	.state("about", {
		url:"/about",
		views: {
			"viewA":{
				templateUrl:"http://localhost/rsc/ui/views/about.html"
			}
		}
	})
	.state("alerts", {
		url:"/alerts",
		views: {
			"viewA":{

				templateUrl:"http://localhost/rsc/ui/views/alerts.html"
			}
		}
	})
	.state("employers", {
		url:"/employers",
		views: {
			"viewA":{
				templateUrl:"http://localhost/rsc/ui/views/employers.html"
			}
		}
	})
  .state('jobbylocation', {
    url:"/location?locationId",
    controller: function($scope, $stateParams)
    {
      $scope.locationId = $stateParams.locationId;
      console.log('You clicked: ' + $stateParams.locationId);
    },
    views: {
      'viewB': {
        templateUrl:"http://localhost/rsc/ui/views/jobbylocation.html"
      }
    }
  }
   )
   .state('jobsbylocation.detail',
 {
   url: 'jobbylocation/:id',
   templateUrl:"http://localhost/rsc/ui/views/jobbylocation-detail.html"
 })
  .state("rest", {
		url:"/resttests",
		views: {
			"viewA":{
				templateUrl:"http://localhost/rsc/ui/views/rest.html"
			}
		}
	})
	.state("joblistings", {
		url:"/joblistings",
		views: {
			"viewA": {
				templateUrl:"http://localhost/rsc/ui/views/listings.html"
			}
		}
	})
}]);
