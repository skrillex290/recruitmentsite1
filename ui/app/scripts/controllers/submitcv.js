'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:SubmitcvCtrl
 * @description
 * # SubmitcvCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('SubmitcvCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
