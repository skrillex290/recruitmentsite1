'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('JobByLocationController', ['$state','$stateParams','$scope','ProvinceService',function ($state, $stateParams, $scope, ProvinceService) {
    var foo = $stateParams.foo;
    var bar = $stateParams.bar;
    $scope.state = $state.current;
    $scope.params = $stateParams;
   
   $scope.getLocations = function()
    {
      console.log('GetLocations fired: ' + $scope.province_id);
    }
   $scope.getallProvinces = function()
    {
      console.log("Getting data");
      ProvinceService.query(function(data)
      {        
        $scope.provinces = data;
        console.log($scope.provinces);
      });
    }

  }]);
