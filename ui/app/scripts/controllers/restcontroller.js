'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:RestController
 * @description
 * # RestController
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('RestCtrl', ['$scope', 'ProvinceService',function ($scope, ProvinceService) {
    $scope.provinces = [];
    $scope.locations = [];
    $scope.getLocations = function()
    {
      $scope.result = ProvinceService.get(
        {id:$scope.province_id}, function()
        {
          console.log($scope.result);
        }
      );
    }
    $scope.getLocations = function()
    {
      console.log('GetLocations fired');
    }
    $scope.getallProvinces = function()
    {
      console.log("Getting data");
      ProvinceService.query(function(data)
      {        
        $scope.provinces = data;
        console.log($scope.provinces);
      });
    }

  }]);
