'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:ListingsCtrl
 * @description
 * # ListingsCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('ListingsCtrl', ['$scope','JobService',function ($scope, JobService) {   
	
	  $scope.filteredData = [];
	  $scope.currentPage = 0;
	  $scope.numPerPage = 5;
	  $scope.maxSize = 100;
	  
	  $scope.jobs = JobService.query(function(f)
	  {
		 $scope.data= f;
		 $scope.currentPage = 1;
	  });
	  
	  $scope.$watch("currentPage + numPerPage", function() {
		  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
		  var end = begin + $scope.numPerPage;						
		  $scope.filteredData = $scope.jobs.slice(begin, end);
		}			
	);     
  }]);
