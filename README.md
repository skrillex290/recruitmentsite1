# README #

This README would normally document whatever steps are necessary to get your application up and running.
### Check sh-t into source control ###

Assuming you have initially `checkout` the project (follow instructions elsewhere on bitbucket).

    git pull (to get the latest of files other developers have checked in)
    git add . (add new files)
    git commit -m "commit message" 
    git push -u origin master (do a check-in)

You may be prompted to enter the ssh-agent password for the key if it has not been loaded. This is the password on the key, not your linux account password.

### What is this repository for? ###

* Quick summary

This is a space-program type application where the objective is to simply write a professional looking functional application with minimal commercial viability but which demonstrates professional software construction methods. ie. AngularJS, GruntJS workflow, REST api and responsive bootstrap themable UI. Technologies are Flask/Python and Javascript.

The idea is if you're going to pitch for software work you should be able to use this type of application to 'inject' yourself into the industry. The more injections the more reputation we could gain.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Here's the magic of this method of working. You should be able to install the components using `bower install` and `pip install -r requirements.txt` (in the appropriate folder after installing `virtualenv`). Load the test server with 
    ./restapp.py
This will run the api server on `http://localhost:5000`. Execute `http://localhost:5000/api/baseload` to baseload some test data into memory. Test that by going to `http://localhost:5000/jobs` which will load the list of jobs.
Now type `grunt serve` in the `/ui` folder. This will load your browser onto the application's single page `index.html`. Enjoy. I will post a more structured walk-through weather-permitting.

* Configuration

* Dependencies

Preferably a Linux machine with Python 2.7. No need for sudo access. Just install `pip`, then `virtualenv` to install all libraries to your user account.

* Database configuration

None yet.

* How to run tests


* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact