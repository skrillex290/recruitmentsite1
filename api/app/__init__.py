from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
#app.json_encoder = AlchemyEncoder
#http://stackoverflow.com/questions/5022066/how-to-serialize-sqlalchemy-result-to-json?lq=1
from app.json_encoder import AlchemyEncoder
app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
from app import models
