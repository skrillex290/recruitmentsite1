from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
job = Table('job', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('description', String(length=1024)),
    Column('title', String(length=128)),
    Column('location_id', Integer),
    Column('create_date', DateTime),
    Column('expiry_date', Date),
)

job_tags = Table('job_tags', post_meta,
    Column('job_id', Integer, primary_key=True, nullable=False),
    Column('tag_id', Integer, primary_key=True, nullable=False),
)

location = Table('location', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=128)),
    Column('province_id', Integer),
)

province = Table('province', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=1024)),
)

tag = Table('tag', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=32)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['job'].create()
    post_meta.tables['job_tags'].create()
    post_meta.tables['location'].create()
    post_meta.tables['province'].create()
    post_meta.tables['tag'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['job'].drop()
    post_meta.tables['job_tags'].drop()
    post_meta.tables['location'].drop()
    post_meta.tables['province'].drop()
    post_meta.tables['tag'].drop()
