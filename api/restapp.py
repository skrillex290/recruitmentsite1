#!flask/bin/python

from flask import Flask, request
from flask_restful import Resource, Api, abort, reqparse
from flask.ext.cors import CORS, cross_origin
from puts import get_province_names, get_random_city, get_tags, get_job_descriptions, get_job_titles, get_job_levels
from app import db, models
from flask import request, json, jsonify, abort
from flask.json import JSONEncoder
import calendar
from dotmap import DotMap
import random
from datetime import datetime, timedelta

app = Flask(__name__)
api = Api(app)

################################################################
jobs = []
def findjob(index):
	if index < len(jobs)-1:
		return True
	else:
		return False

def abort_if_job_doesnt_exist(id):
	if not findjob(id):
		abort(404, message="Job {} does not exist".format(id))
################################################################################
def get__and_abort_if_province_doesnt_exist(id):
	j = models.Province.query.get(id)
	if j == None:
		print "Item not found,"
		return abort(404, message="Province with id {} does not exist".format(id))
	else:
		j = j.to_dict(show=["id","name","locations"])
		print j
		for i in range(len(j["locations"])):
			loc_name = models.Location.query.get(j["locations"][i]["id"]).name
			print loc_name
			j["locations"][i]["location"] = loc_name
	return jsonify(result=j)

class JProvince(Resource):
	def get(self, id):
		return get__and_abort_if_province_doesnt_exist(id)

	def get(self,id=None):
		if not id:
			r = models.Province.query.all()
		else:
			return get__and_abort_if_province_doesnt_exist(id)
		n = [p.to_dict(show=['id','name']) for p in r]
		return jsonify(result=n)

	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument('name', help="name argument could not be found.", required=True)
		args = parser.parse_args()
		u = models.Province(name=args["name"])
		db.session.add(u)
		db.session.commit()
		return u.id, 200
################################################################################
def get__and_abort_if_location_doesnt_exist(id):
	j = models.Location.query.get(id)
	if j == None:
		abort(404, message="Location with id {} does not exist".format(id))
	else:
		return jsonify(result=j.to_dict(show=["id","name","province"]))
class JLocation(Resource):
	def get(self, id):
		return get__and_abort_if_location_doesnt_exist(id)
	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument('name', help="name argument could not be found.", required=True)
		parser.add_argument('province_id', help="province_id argument could not be found.", required=True)
		args = parser.parse_args()
		p = models.Province.query.get(args["province_id"])
		u = models.Location(name=args["name"], province=p)
		db.session.add(u)
		db.session.commit()
		return u.id, 200

################################################################################
def get__and_abort_if_fruit_doesnt_exist(id):
	j = models.Fruit.query.get(id)
	if j == None:
		abort(404, message="Fruit with id {} does not exist".format(id))
	else:
		return jsonify(result=j.to_dict(show=["name","description"]))
class JFruit(Resource):
	def get(self, id):
		return get__and_abort_if_fruit_doesnt_exist(id)
	def put(self):
		parser = reqparse.RequestParser()
		parser.add_argument('name', help="level argument could not be found.", required=True)
		parser.add_argument('description', help="title argument could not be found.", required=True)
		args = parser.parse_args()
		u = models.Fruit(name=args["name"], description=args["description"])
		db.session.add(u)
		db.session.commit()
		return u.id, 200
################################################################################
class Job(Resource):
	def get(self, id=None):
		if id is not None:
			return abort_if_job_doesnt_exist(id)
		else:
			j = models.Job.query.all()
			return jsonify(result=[p.to_dict(show=["id","name","title","description"]) for p in j])
	def delete(self,id):
		abort_if_job_doesnt_exist(id)
		del jobs[id]
		return '', 204
	def put(self, id):
		parser = reqparse.RequestParser()
		parser.add_argument('level', help="level argument could not be found.", required=True)
		parser.add_argument('title', help="title argument could not be found.", required=True)
		parser.add_argument('location', help="location argument could not be found.", required=True)
		parser.add_argument('description', help="description argument could not be found.", required=False)
		parser.add_argument('category', help="category argument could not be found.", required=True)
		args = parser.parse_args()
		args["id"]=len(jobs)
		jobs.append(args)
		return str(args), 200
class ApiUtils(Resource):
	def get(self, action):
		if action=="baseload":
			for i in range(100):
				jobs.append(getjobtitle())
		elif action=="loadjobs":
			locations = models.Location.query.all()
			tags = models.Tag.query.all()
			job_descriptions = get_job_descriptions()
			job_titles = get_job_titles()
			job_levels = get_job_levels()
			create_date = datetime.now()
			for l in locations:
				for i in range(random.choice(range(0,100))):
					expiry_date = datetime.now() + timedelta(days=random.choice(range(10,30))) #10 to 30 days from now
					p = models.Job(location_id=l.id,create_date=create_date, expiry_date=expiry_date, title=random.choice(job_titles), description=random.choice(job_descriptions))
					p.tags = random.sample(tags,random.choice(range(1,5)))
					db.session.add(p)
			db.session.commit()
		elif action=="loadprovinces":
			jc = get_province_names()
			for j in jc:
				p = models.Province(name=j)
				db.session.add(p)
			db.session.commit()
		elif action=="loadtags":
			tc = get_tags()
			for t in tc:
				p = models.Tag(name=t)
				db.session.add(p)
			db.session.commit()
		elif action=="loadcities": #generate 200 locations
			jc = models.Province.query.all()
			for i in range(200):
				c = models.Location(name=get_random_city(),province_id=random.choice(jc).id)
				db.session.add(c)
			db.session.commit()
		else:
			abort(400, message="Action not understood")
		return 'Ok',200

class JobByLocation(Resource):
	def get(self, locationid):
		jobs = models.Job.query.filter_by(location_id=1).all()
		data = {}
		for j in jobs:
			w = j.to_dict(show=["id","title","description","expiry_date","tags"])
			for i in range(len(w["tags"])):
					tag_name = models.Tag.query.get(w["tags"][i]["id"]).name
					w["tags"][i]["name"] = tag_name
			data[w["id"]]= w
		return jsonify(data)


@app.after_request
def after_request(response):
	response.headers.add('Access-Control-Allow-Origin', '*')
	response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
	response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
	return response

api.add_resource(JobByLocation,'/jobsbylocation/<int:locationid>')
api.add_resource(Job, '/job/<int:id>','/jobs')
api.add_resource(ApiUtils, '/api/<string:action>')
api.add_resource(JFruit, '/fruit/<int:id>', '/fruit')
#####################################################
api.add_resource(JProvince, '/province/<int:id>', '/province')
api.add_resource(JLocation, '/location/<int:ids>', '/location')
#####################################################
if __name__ == '__main__':
	app.run(debug=True)
