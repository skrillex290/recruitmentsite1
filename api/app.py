#!flask/bin/python
from flask import Flask, jsonify
from rikeripsum import rikeripsum
from flask.ext.cors import CORS, cross_origin
app = Flask(__name__)

    
def generateJobListing():
    listing = {
               'title':'C# Developer',
               'description':rikeripsum.generate_paragraph(),
               'location':'Cape Town',
               'open':False
               }
    return listing
def getListings():
    listings = []
    for i in range(100):
        listings.append(generateJobListing())
    return listings


@app.route('/eightball/api/v1.0/listings', methods=['GET'])
@cross_origin()
def get_tasks():
    return jsonify({'listings':getListings()})

if __name__ == '__main__':
    app.run(debug=True)
